# XiChinkPink

World of Warcraft 2.4.3 addon.

## What it does?

It removes messages that contain non-ascii characters from public channels.  
Now you can enjoy clean chat without having to `/ignore` individuals.

