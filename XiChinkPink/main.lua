---@diagnostic disable: undefined-global
local frame = CreateFrame( "FRAME", "XiChinkPinkFrame" )

local function is_non_ascii( text )
  if not text then return false end

  for i = 1, #text do
    if text:byte( i ) > 127 then return true end
  end

  return false
end

local function on_event( _, event )
  if event == "PLAYER_LOGIN" then
    ChatFrame_AddMessageEventFilter( "CHAT_MSG_CHANNEL", is_non_ascii )
    ChatFrame_AddMessageEventFilter( "CHAT_MSG_EMOTE", is_non_ascii )
    ChatFrame_AddMessageEventFilter( "CHAT_MSG_SAY", is_non_ascii )
    ChatFrame_AddMessageEventFilter( "CHAT_MSG_WHISPER", is_non_ascii )
    ChatFrame_AddMessageEventFilter( "CHAT_MSG_YELL", is_non_ascii )
    DEFAULT_CHAT_FRAME:AddMessage( "|cff209ff9XiChinkPink|r: Loaded." )
  end
end

frame:RegisterEvent( "PLAYER_LOGIN" )
frame:SetScript( "OnEvent", on_event )
